$('./body') {
  
	$("./div[@data-role='page']") {
		$("./div[@data-role='content']") {
			
			# Remove accessibility content
			$("./*[@class='accessibility']") {
				remove()	
			}

			$("./div[@id='container']") {
				$("./div[1]") {
					$("./div[@id='header_top']") {
						
						$(".//img[@class='logo_header']") {

							# Replace upstream logo image with smaller one
							attribute("src", asset("images/logo.png") )

						}

						# Remove tagline
						$("./img[@class='tagline']") {
							remove()
						}

						# Inser panel button
						insert_bottom("a", class: "mw-panel-btn", href: "#mw-panel", "panel"){

						}

					}
				}
			}
			

		}
	}

}
