$("./body") {

	# Remove all tag styles
	$(".//*") {
		remove("@style")
	}

	# Wrap whole page
	insert_top("div"){
		attributes(data-role: "page")
		add_class("mw-main-wrap")	

		# Insert main content container
		insert_top("div"){
			attributes(data-role: "content")
			add_class("jqm-content")	

			# Move content into main container
			move_here("../../*[position() > 1]", "bottom"){

				$("./div[@id='sidebar']|./div[@id='sidebar_landing']") {
					
					# Move alert box into footer
					$("./div[@id='alert_box']") {					
						move_to("../../div[@id='footer']/div[@id='footer_content']", "bottom")
					}

					# Move social media links into footer
					$("./div[@id='social_media']") {
						move_to("../../div[@id='footer']/div[@id='footer_content']", "top")
					}

					# Move contact information into footer
					$("./div[@id='contact_information']") {
						move_to("../../div[@id='footer']/div[@id='footer_content']", "bottom")	
					}

					# Move idaho logo into footer
					$("./a") {
						move_to("../../div[@id='footer']/div[@id='footer_content']", "top")
					}

					# Remove rest of content from sidebar
					remove()

				}

			}
			
		}

		# Insert right slide panel container
		insert_top("div", data-role: "panel", id: "mw-panel", data-display: "reveal", data-position:"right"){

			# Move main menu into panel
			move_here("../div[2]/div[@id='container']/div[1]/div[@id='menu_vert']", "bottom"){
				
				# Remove navigation title
				$("./h2") {
					remove()
				}

				$("./div[@id='menuwrapper']") {
					$("./ul[@id='primary-nav']") {
						$("./li") {							

							# Create unique name for every menu category item and store it in variable
							$index = "tg-" + index()

							$("./a") {

								# Save category URL into a variable
								$categoryLink = fetch("@href")								

								# Remove 'href' attribute
								remove("@href")

								# Add twitter bootstrap attributes to top <a> element to make it button
								attributes(data-toggle:"collapse", data-target:"#" + $index)		
								add_class("btn collapsed")

							}

							$("./ul") {

								# Add twitter bootstrap attributes to top <a> element to make it container
								attributes(id: $index)
								add_class("collapse")

								# Insert new <li> tag
								insert_top("li"){

									# Insert new <a> tag and assign value from variable to its 'href' attribute
									insert("a", href: $categoryLink){

										# Insert new <span> tag
										insert("span"){

											# Insert text
											text("View All")

										}
									}
								}															

							}

						}
					}
				}

			}

			# Insert bottom logo container
			insert("div", class: 'mw-panel-logo'){
				
				# Insert bottom logo link. Href attribute is mix of static text and value of global variable '$host'
				insert("a", href: "http://" + $host)

			}

		}

	}
				
}