$("./body") {

	$("./div[@data-role='page']") {
		$("./div[@data-role='content']") {
			$("./div[@id='container']") {
				$("./div[@id='footer']") {
					$("./div[@id='footer_content']") {
						
						# remove unwantet content
						remove_text_nodes()
						$("./a|./br") {
							remove()
						}

						$("./div[@id='alert_box']") {
							
							move_to("..", "top")

							$("./form") {
								
								# Add class for flexibile box container
								add_class("mw-flex-box")

								# Wrap text inside <span> tag
								wrap_text_children("span", class: "mw-title"){

									# Move <span> outside <form>
									move_to("../..", "top")

								}

								$("./input[@type='text']") {

									# Add class for flexible box responsive element
									add_class("mw-flex-box-item-1")

									attribute("placeholder", "Enter email...")

								}

							}

						}

						$("./div[@id='social_media']") {
							# Move social media <div> to top of the footer
							move_to("..", "top")

							# Remove <br> tags
							remove("./br")

							$("./a[1]") {
								$("./img") {

									# Replace upstream facebook icon
									attribute("src", asset("images/fb.png") )

								}
							}

							$("./a[2]") {
								$("./img") {

									# Replace upstream twitter icon
									attribute("src", asset("images/tw.png") )

								}
							}

							$("./a[3]") {
								$("./img") {

									# Replace upstream rss icon
									attribute("src", asset("images/rss.png") )

								}
							}
						}

						
						$("./div[@id='contact_information']") {
							# Set twitter bootstrap collapse container attributes
							attributes(id: "mw-contact-c")
							add_class("collapse")

							# Set twitter bootstrap collapse button attributes
							insert_bottom("a", class: "mw-close"){
								attributes(data-toggle:"collapse", data-target:"#mw-contact-c")
								add_class("btn btn-danger mw-contact")
							}


							# Insert new <div>
							insert_bottom("div"){

								# Move content into twitter bootstrap container
								move_here("../*[position() < last()]", "bottom")
							
							}

						}

						# Insert conatainer <div>
						insert_bottom("div", class: "mw-f-container"){
							
							# Insert contact <a> tag
							insert_top("a"){

								# Set twitter bootstrap collapse button attributes
								attributes(data-toggle:"collapse", data-target:"#mw-contact-c")
								add_class("btn btn-danger mw-contact")

								# Insert text
								text("Contact")								
							}

							# Insert 'visit idaho' link
							insert_bottom("a", class: "mw-idaho", href: "http://www.visitidaho.org/")

						}

						# Insert <p> tag with copyright text 
						insert_bottom("p", class: "mw-copy"){
							text("Copyright © 2007-2013, McCall Chamber of Commerce. ")
						}

						# Insert <div> wrapper
						insert_bottom("div", class: "mw-wrap"){

							# Insert link and point it to desktop site
							insert("a", class: "mw-desktop", "Desktop site", href: 'http://www.mccallchamber.org/')
							
							# Insert <div> wrapper
							insert("div", class: "mw-moovweb"){

								# Insert <span> with text
								insert("span", "by")

								# Insert link and point it to moovwebs site
								insert("a", href: "http://www.moovweb.com"){

									# Insert moovweb logo image
									insert("img", src: asset("images/moovweb.png"))	

								}

							}

						}

					}
				}
			}
		}
	}

}
