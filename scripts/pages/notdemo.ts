$("./head") {
	$("./style") {
		remove()
	}
	$("./script") {
		remove()
	}
	$("./link") {
		remove()
	}

	add_assets()

	# $("./title") {
	# 	$pagetitle = fetch("text()")	
	# }
	
	insert_bottom("style"){
		inner('
			body{
				background: #101010;
			}
			[data-role="page"]{
				background: #101010;	
			}
			.mw_notdemo{
			background-color: #101010;
			text-align: center;
			font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
			color: #dde2e6;
			text-shadow: 1px 1px 0px #000;
			}

			h1{
				font-size: 28px;
				font-weight: bold;
				margin-bottom: 10px;	
				margin-top: 20px;
			}
			h2{
				font-size: 18px;
				font-weight: bold;				
				margin: 5px 20px;
			}
			h3{
				font-size: 13px;
				color: #9E9E9E;
				margin: 0px 20px;
			}
			p{
				color: #b7b7b7;
				font-size: 13px;
				margin-bottom: 30px;
				margin-top: 20px;
			}
			p a{
				color: #e8e8e8;
			}
		')
	}
}
$("./body") {
	add_class("mw_notdemo")
	inner("")
	insert("h1", "Sorry!")
	insert("h2", "The page you have selected is out of scope of this demo.")
	insert("img", class: 'mw_warning', src: asset("images/notdemo-warning.png"), width: '128px', height: '128px')
	insert("p"){
		inner("Please <a>contact</a> Moovweb for more details.")
		$("./a") {
			attribute("href", "http://www.moovweb.com/contact_us")
			attribute("target", "_blank")
		}
	}	
	insert("a"){
		attribute("href", "http://www.moovweb.com")
		attribute("target", "_blank")
		insert("img", class: 'mw_moovweb', src: asset("images/notdemo-logo.png"), width: '121px', height: '15px')
	}	
}