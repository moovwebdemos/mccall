# Place holder/example file
$("./body") {
  add_class("mw-home")

  $("./div[@data-role='page']") {
  	$("./div[@data-role='content']") {
  		$("./div[@id='container']") {

  			$("./div[@id='header_main']") {
  				$("./div[@id='seasonal_container']") {  					
  					$("./div[@class='clickable']") {  						
  						$("./div[1]") {
  							$("./img[1]") {
  								
  								# Move first image to outer container
  								move_to("../..", "bottom")

  							}

  							# Remove other images
  							remove()

  						}

  						$("./div[1]") {  							
  							$("./div") {  								
  								$("./a") {  									

  									# Save URL into a variable
  									$link = fetch("@href")

  									# Move span to outer container
  									$("./span") {
  										move_to("../../../..", "bottom")
  									}

  								}
  							}

  							# remove remains content
								remove()

  						}

  						# Change <div> into <a>
  						name("a")

  						# Add href attribute and point it to value from variable
							attribute("href", $link)

  					}

  					$("./a[1]|./a[3]") {
  						# Add class to first and third <a> tag
  						add_class("mw-odd")
  					}

  				}
  			}

  			$("./div[@id='mainContent_landing']") {
  				
  				# Remove everything after welcome text
  				$("./*[position() > 1]") {
  					remove()
  				}

  				# Copy navigation here from panel
  				copy_here("../../../div[@id='mw-panel']/div[@id='menu_vert']", "bottom"){
            $("./div") {
              $("./ul") {
                $("./li") {
                  $("./ul") {
                    $("./li[1]") {
                      $("./a") {
                                                
                        # Take 'href' value attribute of 'View all' menu item and place it in variable
                        $catlink = fetch("@href")

                      }
                    }

                    # Remove submenu
                    remove()

                  }

                  $("./a") {

                    # Create link to all category
                    attribute("href", $catlink)

                    # Remove twitter bootstrap button attributes
                    remove("@data-target")
                    remove("@data-toggle")

                  }

                }
              }
            }
          }

  			}

  		}
  	}
  }

}
