$("./body") {
	add_class("mw-recreation")

	$("./div[@data-role='page']") {
		$("./div[@data-role='content']") {
			$("./div[@id='container']") {
				
				$("./div[@id='header_season_1']") {

					$("./img") {

						# Wrap <img> with <div> tag and assign class
						wrap("div", class: "mw-image-window")

					}					

					$("./div[@id='sub_nav_container']") {

						# Move breadcrumbs container after top header 
						move_to("../div[@id='header_top']", "after")

						$("./div[@class='crbk']") {
							name("table")
							$("./div[@class='breadcrumbs']") {
								name('tr')
								
								# Wrap all text with <span> and assign class
								wrap_text_children("span", class: "mw-arrow")

								# Remove last arrow
								$("./span[@class='mw-arrow'][last()]") {
									remove()
								}

								# Insert 'home' link at the beggining of breadcrumbs
								insert_top("a", href: "http://" + $host , class: "mw-home")

								# Wrap all elements in <td> tag
								$("./*") {
									wrap("td")
								}

								# Remove last <td> tag
								$("./td[last()]") {
									remove()
								}

							}
						}

					}

				}

				$("./div[@id='mainContent']") {
					
					# Remove print icon
					$("./div[1]") {
						remove()
					}
					
					# Find all images that will be processed to canvas and resize them
					$(".//img[contains(@class, 'itxtalt')]") {
						attribute("width", "140")
						attribute("height", "143")
					}

				}

			}
		}
	}

}